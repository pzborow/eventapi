from django.apps import AppConfig


class TenantOnlyConfig(AppConfig):
    name = "events"
